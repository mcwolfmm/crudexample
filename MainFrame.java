import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class MainFrame extends JFrame {

    private JTextField textField = new JTextField(20);
    private JButton loadButton = new JButton("Load");

    private JTable table = new JTable(new InvoiceTableModel());

    private JButton saveButton = new JButton("Save");

    private JButton addButton = new JButton("Add");
    private JButton deleteButton = new JButton("Delete");

    public MainFrame() throws HeadlessException {
        super("CRUD Example");
        createGUI();
    }

    private void createGUI() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout(5, 25));

        loadButton.addActionListener(this::load);

        addButton.addActionListener(this::add);
        addButton.setEnabled(false);

        deleteButton.addActionListener(this::delete);
        deleteButton.setEnabled(false);

        saveButton.addActionListener(this::save);
        saveButton.setEnabled(false);

        JPanel headerPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 0));
        headerPanel.add(new JLabel("Invoice ID:"));
        headerPanel.add(textField);
        headerPanel.add(loadButton);

        JScrollPane scrollPane = new JScrollPane(table);

        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 0));
        bottomPanel.add(addButton);
        bottomPanel.add(deleteButton);
        bottomPanel.add(Box.createHorizontalStrut(20));
        bottomPanel.add(saveButton);

        add(headerPanel, BorderLayout.PAGE_START);
        add(scrollPane, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.PAGE_END);

        pack();
        setLocationRelativeTo(null);
    }

    private void add(ActionEvent event) {
        Invoice invoice = new Invoice();

        invoice.setDescription("Article");
        invoice.setQty(1.0);
        invoice.setUnitPrice(0.0);

        ((InvoiceTableModel)table.getModel()).add(invoice);
    }

    private void delete(ActionEvent event) {
        int row = table.getSelectedRow();
        if(row == -1) {
            return;
        }

        row = table.convertRowIndexToModel(row);
        ((InvoiceTableModel)table.getModel()).remove(row);
    }

    private void save(ActionEvent event) {
        try {
            long invoice_id = Long.parseLong(textField.getText().trim());
            ((InvoiceTableModel) table.getModel()).store(invoice_id);
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void load(ActionEvent event) {
        try {
            long invoice_id = Long.parseLong(textField.getText().trim());
            ((InvoiceTableModel) table.getModel()).load(invoice_id);

            addButton.setEnabled(true);
            deleteButton.setEnabled(true);
            saveButton.setEnabled(true);
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new MainFrame().setVisible(true));
    }
}
