import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class InvoiceTableModel extends AbstractTableModel implements TableModel {

    private List<Invoice> list;

    public void add(Invoice invoice) {
        if(list == null) {
            list = new ArrayList<>();
        }

        list.add(invoice);
        fireTableRowsInserted(list.size(), list.size());
    }

    public void remove(int row) {
        list.remove(row);
        fireTableRowsDeleted(row, row);
    }

    @Override
    public int getRowCount() {
        if(list == null) {
            return 0;
        }

        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return column > 0 && column < 4;
    }

    @Override
    public Object getValueAt(int row, int column) {
        Invoice invoice = list.get(row);
        switch (column) {
            case 0: return row + 1;
            case 1: return invoice.getDescription();
            case 2: return invoice.getQty();
            case 3: return invoice.getUnitPrice();
            case 4: return invoice.getTotal();
        }

        return null;
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        Invoice invoice = list.get(row);
        switch (column) {
            case 1:
                invoice.setDescription(value.toString());
                break;

            case 2:
                invoice.setQty(Double.valueOf(value.toString()));
                break;

            case 3:
                invoice.setUnitPrice(Double.valueOf(value.toString()));
                break;
        }

        fireTableRowsUpdated(row, row);
    }

    public void load(long invlice_id) {
        SwingWorker<List<Invoice>, Void> worker = new SwingWorker<List<Invoice>, Void>() {
            @Override
            protected List<Invoice> doInBackground() throws Exception {
                return new Database().load(invlice_id);
            }

            @Override
            protected void done() {
                try {
                    list = get();
                    fireTableDataChanged();
                }
                catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        };

        worker.execute();
    }

    public void store(long invoice_id) {
        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                new Database().save(invoice_id, list);
                return null;
            }

            @Override
            protected void done() {
                try {
                    get();
                }
                catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        };

        worker.execute();
    }
}
