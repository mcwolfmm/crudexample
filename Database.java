import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Database {
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://192.168.40.5:3306/test", USERNAME, PASSWORD);
    }

    public List<Invoice> load(long invoice_id) throws SQLException {
        String query = "select * from `invoice` where `invoice_id` = ?";
        try(PreparedStatement statement = getConnection().prepareStatement(query)) {
            statement.setLong(1, invoice_id);

            try(ResultSet rs = statement.executeQuery()) {
                List<Invoice> list = new ArrayList<>();

                while (rs.next()) {
                    Invoice invoice = new Invoice();
                    invoice.setId(rs.getLong("id"));
                    invoice.setDescription(rs.getString("description"));
                    invoice.setQty(rs.getDouble("qty"));
                    invoice.setUnitPrice(rs.getDouble("unitprice"));

                    list.add(invoice);
                }

                return list;
            }
        }
    }

    public boolean save(long invoice_id, List<Invoice> list) throws SQLException {
        try(Connection connection = getConnection()) {
            try {
                connection.setAutoCommit(false);

                String query =
                        "create temporary table if not exists `invoice_tmp` (" +
                                "`id` int(10) unsigned NOT NULL," +
                                "`description` varchar(255) DEFAULT NULL," +
                                "`qty` double DEFAULT NULL," +
                                "`unitprice` double DEFAULT NULL)";

                connection.createStatement().executeUpdate(query);

                query = "insert into `invoice_tmp` values (?, ?, ?, ?)";
                PreparedStatement statement = connection.prepareStatement(query);
                for(Invoice invoice: list) {
                    statement.setLong(1, invoice.getId());
                    statement.setString(2, invoice.getDescription());
                    statement.setDouble(3, invoice.getQty());
                    statement.setDouble(4, invoice.getUnitPrice());

                    statement.addBatch();
                }

                statement.executeBatch();
                statement.close();

                query =
                        "delete invoice from invoice " +
                        "left join invoice_tmp on (invoice.id = invoice_tmp.id) " +
                        "where invoice_id = ? and invoice_tmp.id is null";

                statement = connection.prepareStatement(query);
                statement.setLong(1, invoice_id);
                statement.executeUpdate();
                statement.close();

                query =
                        "update `invoice` " +
                        "join `invoice_tmp` using (`id`) " +
                        "set " +
                                "`invoice`.description = `invoice_tmp`.description, " +
                                "`invoice`.qty = `invoice_tmp`.qty, " +
                                "`invoice`.unitprice = `invoice_tmp`.unitprice";

                connection.createStatement().executeUpdate(query);

                query =
                        "insert into `invoice` (`invoice_id`, `description`, `qty`, `unitprice`) " +
                        "select ? as `invoice_id`, `description`, `qty`, `unitprice` from `invoice_tmp` where `id` = 0";

                statement = connection.prepareStatement(query);
                statement.setLong(1, invoice_id);
                statement.executeUpdate();
                statement.close();

                connection.createStatement().executeUpdate("drop table if exists `invoice_tmp`");

                connection.commit();
                return true;
            }
            catch (Exception e) {
                connection.rollback();
                throw e;
            }
        }
    }
}
